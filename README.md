# Guides

To make it easy for you, here's a list of recommended guides!


## AI Impact Assessment

Het AI Impact Assessment (AIIA) is een hulpmiddel voor het maken van afwegingen bij het inzetten van kunstmatige intelligentie (artificial intelligence, AI) in een project. Het AIIA dient als instrument voor het gesprek en het vastleggen van het denkproces zodat onder andere de verantwoording, kwaliteit en reproduceerbaarheid worden vergroot.

- [Website AIIA](https://www.rijksoverheid.nl/documenten/rapporten/2022/11/30/ai-impact-assessment-ministerie-van-infrastructuur-en-waterstaat)
- [Download AIIA document](https://www.rijksoverheid.nl/binaries/rijksoverheid/documenten/rapporten/2022/11/30/ai-impact-assessment-ministerie-van-infrastructuur-en-waterstaat/ienw-ai-impact-assessment-v2.pdf)


## Gitlab omgeving andere IenW organisaties
- [RWS Datalab](https://gitlab.com/rwsdatalab)
